<?php

declare(strict_types=1);

namespace FlixTech;

interface FibonacciInterface
{
    public function getNumber(int $n): int;
}
