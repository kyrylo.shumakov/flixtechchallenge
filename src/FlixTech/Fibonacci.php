<?php

declare(strict_types=1);

namespace FlixTech;

class Fibonacci implements FibonacciInterface
{
    public function getNumber(int $n): int
    {
        $this->validate($n);

        return $this->calculate($n);
    }

    private function calculate(int $n): int
    {
        if ($n === 1) {
            return 0;
        }

        if ($n === 2) {
            return 1;
        }

        return $this->calculate($n-1) + $this->calculate(($n-2));
    }

    private function validate(int $n): void
    {
        if ($n < 1) {
            throw new \InvalidArgumentException("Input param has to be greater than 0");
        }
    }
}
