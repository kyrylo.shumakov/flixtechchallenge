<?php

declare(strict_types=1);

namespace FlixTech\Command;

use FlixTech\FibonacciInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FibonacciCalculateCommand extends Command
{
    /**
     * @var FibonacciInterface
     */
    private $calculator;

    public function __construct(FibonacciInterface $calculator)
    {
        $this->calculator = $calculator;

        parent::__construct('flixtech:fibonacci-calculate');
    }

    protected function configure(): void
    {
        $this
            ->addArgument('number');
    }

    public function getDescription()
    {
        return 'Calculate n-th Fibonacci number';
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $number = $this->parseNumber($input);
        } catch (\InvalidArgumentException $exception) {
            $output->write($exception->getMessage());

            return Command::FAILURE;
        }

        try {
            $result = $this->calculator->getNumber($number);
        } catch (\Exception $e) {
            $output->write(sprintf("Couldn't calculate Fibonacci number. Error message: %s", $e->getMessage()));

            return Command::FAILURE;
        }

        $output->writeln(sprintf("Fibonacci number for index \"%d\" is \"%d\"", $number, $result));

        return Command::SUCCESS;
    }

    private function parseNumber(InputInterface $input): int
    {
        $number = $input->getArgument('number');

        if (!is_string($number)) {
            throw new \InvalidArgumentException("Number parameter must be a string");
        }

        if (!is_numeric($number)) {
            throw new \InvalidArgumentException("Number parameter must be numeric");
        }

        return (int) $number;
    }
}
