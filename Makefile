ssh:
	docker-compose exec flixtech flixtech sh

composer-install:
	docker-compose exec flixtech composer install

up:
	docker-compose up -d

down:
	docker-compose down

build: up composer-install

test:
	docker-compose exec flixtech bin/phpunit --testsuite UnitTest

code-quality:
	docker-compose exec flixtech  bin/phpstan analyse

code-style:
	docker-compose exec flixtech  bin/php-cs-fixer fix --diff --dry-run src/

me-fibonacci-for:
	docker-compose exec flixtech php bin/console flixtech:fibonacci-calculate ${number}