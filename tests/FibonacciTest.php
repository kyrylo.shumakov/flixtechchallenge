<?php

declare(strict_types=1);

namespace FlixTech\Test;

use FlixTech\Fibonacci;
use PHPUnit\Framework\TestCase;

class FibonacciTest extends TestCase
{
    /**
     * @dataProvider invalidNumbersProvider
     */
    public function testGetNumberFail(int $number): void
    {
        static::expectException(\InvalidArgumentException::class);
        (new Fibonacci())->getNumber($number);
    }

    /**
     * @dataProvider getNumberDataProvider
     */
    public function testGetNumber(int $number, int $expectedValue): void
    {
        static::assertEquals($expectedValue, (new Fibonacci())->getNumber($number));
    }

    /**
     * @return array<array>
     */
    public function invalidNumbersProvider(): array
    {
        return [
            [0],
            [-1],
        ];
    }

    /**
     * @return array<array>
     */
    public function getNumberDataProvider(): array
    {
        return [
            [
                1,
                0,
            ],
            [
                2,
                1,
            ],
            [
                3,
                1,
            ],
            [
                5,
                3,
            ],
            [
                6,
                5,
            ],
            [
                7,
                8,
            ],
        ];
    }
}